<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KJ_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('core/vendor-model/m_vendor');

        $install_status = $this->m_vendor->view_vendor_data()->result();

        if ($install_status == null) {
            redirect(base_url() . 'install/');
        }
    }
}

class AdminInterface extends KJ_Controller {

    public function __construct() {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');
        $role = $this->session->userdata('role');

        if (!$logged_in) {
            redirect(base_url() . 'login/');
        } else {
            if ($role == "admin") {
                $data['username'] = $this->session->userdata('username');
                $data['role'] = $role;
                $this->load->view('inc/header');
                $this->load->view('inc/menu',$data);
            } else {
                redirect(base_url() . 'user/dashboard/');
            }
        }
    }
}

class UserInterface extends KJ_Controller {

    public function __construct() {
        parent::__construct();
        
        $logged_in = $this->session->userdata('logged_in');
        $role = $this->session->userdata('role');

        if (!$logged_in) {
            redirect(base_url() . 'login/');
        } else {
            if ($role == "user") {
                $data['username'] = $this->session->userdata('username');
                $data['role'] = $role;
                $this->load->view('inc/header');
                $this->load->view('inc/menu',$data);
            } else {
                redirect(base_url() . 'admin/dashboard/');
            }
        }
    }
}
