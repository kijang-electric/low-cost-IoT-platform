<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_user.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Card Monitoring Devices</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage Devices</a></li>
                <li class="breadcrumb-item active">List Devices</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Your Device List</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover ">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Name</th>
                                    <th>Serial</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php 
                                    $no = 1;
                                    foreach($devices as $d){ 
                                ?>

                                <tr>
                                    <th scope="row"><?php echo $no++ ?></th>
                                    <td><?php echo $d->product ?></td>
                                    <td><span class="badge badge-primary"><?php echo $d->serial ?></span></td>
                                    <td>
                                        <a href="<?php echo base_url(). 'user/cardmon/details/' .$d->serial; ?>"><button type="button" class="btn btn-success m-b-10 m-l-5">Monitoring</button></a>
                                    </td>
                                </tr>
                                
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>