<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_user.php');
?>

<div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Monitoring Card View</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Monitoring</a></li>
                        <li class="breadcrumb-item active">Sample Card</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-3">
                        <div class="card bg-primary p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span><i class="ti-bag f-s-40"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white" id="data1">0</h2>
                                    <p class="m-b-0">Data 1</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-pink p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span><i class="ti-comment f-s-40"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white" id="data2">0</h2>
                                    <p class="m-b-0">Data 2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-success p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span><i class="ti-vector f-s-40"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white" id="data3">0</h2>
                                    <p class="m-b-0">Data 3</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-danger p-20">
                            <div class="media widget-ten">
                                <div class="media-left meida media-middle">
                                    <span><i class="ti-location-pin f-s-40"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2 class="color-white" id="data4">0</h2>
                                    <p class="m-b-0">Data 4</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-title">
                                <h4>MQTT Server Info</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Url</label>
                                    <input class="form-control" id="url" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Port</label>
                                    <input class="form-control" id="port" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>TLS</label>
                                    <input class="form-control" id="tls" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Topic</label>
                                    <input class="form-control" id="topic" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <input class="form-control" id="status" type="text" disabled="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-12">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2 id="serial">null</h2>
                                        <p class="m-b-0">Serial</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2 id="ip">null</h2>
                                        <p class="m-b-0">IP Address</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-archive f-s-40 color-warning"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2 id="mac">null</h2>
                                        <p class="m-b-0">Mac Address</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
    $this->load->view('inc/footer.php');
?>