<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_user.php');
?>

<div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Monitoring List View</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Monitoring</a></li>
                        <li class="breadcrumb-item active">Sample List</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-title">
                                <h4>MQTT Server Info</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Url</label>
                                    <input class="form-control" id="url" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Port</label>
                                    <input class="form-control" id="port" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>TLS</label>
                                    <input class="form-control" id="tls" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Topic</label>
                                    <input class="form-control" id="topic" type="text" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <input class="form-control" id="status" type="text" disabled="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-title">
                                <h4>Incoming Data List</h4>
                            </div>
                            <div class="card-body">
                                <ul id='ws' style="font-family: 'Courier New', Courier, monospace;"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
    $this->load->view('inc/footer.php');
?>