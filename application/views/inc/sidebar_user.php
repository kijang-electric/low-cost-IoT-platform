<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>user/dashboard/">Welcome Message </a></li>
                        <li><a href="<?php echo base_url(); ?>user/dashboard/docs/">Documentation </a></li>
                    </ul>
                </li>
                <li class="nav-label">Monitoring</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Monitoring Type </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>user/listmon/">List Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>user/cardmon/">Card Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>user/chartmon/">Chart Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>user/tablemon/">Table Monitoring</a></li>
                    </ul>
                </li>
                <li class="nav-label">Devices</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-gear"></i><span class="hide-menu">Manage Device </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>user/devices/">List Devices</a></li>
                        <li><a href="<?php echo base_url(); ?>user/devices/register/">Register Device</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>