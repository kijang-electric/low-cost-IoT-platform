<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/dashboard/">Welcome Message </a></li>
                        <li><a href="<?php echo base_url(); ?>admin/dashboard/docs/">Documentation </a></li>
                    </ul>
                </li>
                <li class="nav-label">Monitoring</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Monitoring Type </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/listmon/">List Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/cardmon/">Card Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/chartmon/">Chart Monitoring</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/tablemon/">Table Monitoring</a></li>
                    </ul>
                </li>
                <li class="nav-label">Devices</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-gear"></i><span class="hide-menu">Manage Device </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/devices/">List Devices</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/devices/register/">Register Device</a></li>
                    </ul>
                </li>
                <li class="nav-label">MQTT Server</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-server"></i><span class="hide-menu">Manage MQTT Server </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/mqtt/">List MQTT Server</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/mqtt/register/">Add MQTT Server</a></li>
                    </ul>
                </li>
                <li class="nav-label">Users</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">Manage Users </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/users/">List Users</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/users/admin/">List Admin</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/users/register/">Register Admin</a></li>
                    </ul>
                </li>
                <li class="nav-label">Platform</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-building"></i><span class="hide-menu">Vendor Management </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/vendor/">Vendor Info</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/vendor/license/">License and Agreement</a></li>
                        <li><a href="<?php echo base_url(); ?>admin/vendor/privacy/">Privacy Policy</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>