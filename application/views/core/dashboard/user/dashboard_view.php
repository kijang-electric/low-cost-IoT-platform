<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_user.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Home</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                <li class="breadcrumb-item active">Welcome Message</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Welcome to Low Cost IoT Platform</h4>
                    </div>
                    <div class="card-body">
                        <img src="<?php echo base_url(); ?>assets/images/landing.svg" class="img-responsive" alt="landing-image">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>