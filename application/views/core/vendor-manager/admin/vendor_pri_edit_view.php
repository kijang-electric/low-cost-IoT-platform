<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Edit Privacy and Policy</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Vendor Management</a></li>
                        <li class="breadcrumb-item active">Edit Privacy and Policy</li>
                    </ol>
                </div>
            </div>
            <?php 
                foreach($vendor as $v){ 
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-content">
                                    <form role="form" action="<?php echo base_url(). 'admin/vendor/exec_privacy'; ?>" method="post">
                                        <div class="mt-4">
                                            <h5>Edit Privacy and Policy</h5>
                                            <hr/>
                                                <div class="mt-4">
                                                <input type="hidden" name="v_id" value="<?php echo $v->v_id ?>">
                                                    <div class="form-group">
                                                        <textarea name="v_policy" rows="8" cols="80" class="form-control" style="height:300px"><?php echo $v->v_policy ?></textarea>
                                                    </div>
                                                </div>
                                            <hr/>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-warning waves-effect waves-light w-md m-b-30">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

<?php
    $this->load->view('inc/footer.php');
?>