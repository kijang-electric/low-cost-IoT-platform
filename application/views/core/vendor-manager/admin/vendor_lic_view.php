<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">License and Agreement</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Vendor Management</a></li>
                        <li class="breadcrumb-item active">License and Agreement</li>
                    </ol>
                </div>
            </div>
            <?php 
                foreach($vendor as $v){ 
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-content">
                                    <div class="mt-4">
                                        <h5>License and Agreement</h5>
                                        <hr/>
                                            <?php echo $v->v_license ?>
                                        <hr/>
                                    </div>
                                    <div class="text-right">
                                    <a href="<?php echo base_url(); ?>admin/vendor/license_edit/"><button type="button" class="btn btn-warning waves-effect waves-light w-md m-b-30">Edit</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

<?php
    $this->load->view('inc/footer.php');
?>