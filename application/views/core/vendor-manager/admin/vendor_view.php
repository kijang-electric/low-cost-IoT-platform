<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Vendor Info</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Vendor Management</a></li>
                <li class="breadcrumb-item active">Vendor Info</li>
            </ol>
        </div>
    </div>
    <?php 
        foreach($vendor as $v){ 
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <header>
                                <div class="avatar">
                                    <img src="<?php echo base_url(); ?>assets/images/users.svg" alt="user" class="profile-pic" />
                                </div>
                            </header>
                            <h3><?php echo $v->v_name ?></h3>
                            <div class="desc">
                                <?php echo $v->v_owner ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_email ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Phone</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_phone ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>City</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_city ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Province</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_province ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Country</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_country ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Postcode</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $v->v_postcode ?></p>
                                    </div>
                                </div>
                                <hr>
                                <strong>Address</strong>
                                <br>
                                <p class="m-t-30"><?php echo $v->v_address ?></p>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="<?php echo base_url(). 'admin/vendor/exec_vendor'; ?>" method="post">
                                    <hr>
                                    <input type="hidden" name="v_id" value="<?php echo $v->v_id ?>">
                                    <div class="form-group">
                                        <label class="col-md-12">Vendor Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_name" class="form-control form-control-line" value="<?php echo $v->v_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Owner</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_owner" class="form-control form-control-line" value="<?php echo $v->v_owner ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="v_email" class="form-control form-control-line" value="<?php echo $v->v_email ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_phone" class="form-control form-control-line" value="<?php echo $v->v_phone ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_address" class="form-control form-control-line" value="<?php echo $v->v_address ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">City</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_city" class="form-control form-control-line" value="<?php echo $v->v_city ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Province</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_province" class="form-control form-control-line" value="<?php echo $v->v_province ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Country</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_country" class="form-control form-control-line" value="<?php echo $v->v_country ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Postcode</label>
                                        <div class="col-md-12">
                                            <input type="text" name="v_postcode" class="form-control form-control-line" value="<?php echo $v->v_postcode ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Update Info</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

<?php
    $this->load->view('inc/footer.php');
?>