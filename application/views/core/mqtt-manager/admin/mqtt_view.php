<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">MQTT Server</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage MQTT Server</a></li>
                <li class="breadcrumb-item active">List MQTT Server</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Your MQTT Server List</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover ">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>MQTT Host</th>
                                    <th>MQTT Vendor</th>
                                    <th>Device Serial</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php 
                                    $no = 1;
                                    foreach($mqtt_server as $m){ 
                                ?>

                                <tr>
                                    <th scope="row"><?php echo $no++ ?></th>
                                    <td><?php echo $m->mqtt_host ?></td>
                                    <td><?php echo $m->mqtt_vendor ?></td>
                                    <td><span class="badge badge-primary"><?php echo $m->serial_device ?></span></td>
                                    <td>
                                        <a href="<?php echo base_url(). 'admin/mqtt/details/' .$m->id_mqttserver; ?>"><button type="button" class="btn btn-info m-b-10 m-l-5">Details</button></a>
                                        <a href="<?php echo base_url(). 'admin/mqtt/delete/' .$m->id_mqttserver; ?>"><button type="button" class="btn btn-danger m-b-10 m-l-5">Delete</button></a>
                                    </td>
                                </tr>
                                
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>