<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">MQTT Server</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage MQTT Server</a></li>
                <li class="breadcrumb-item active">Edit MQTT Server</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Fill this form to edit MQTT Server</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                        <?php foreach($mqtt_server as $m){ ?>
                            <form action="<?php echo base_url(). 'admin/mqtt/update'; ?>" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Host</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id_mqttserver" value="<?php echo $m->id_mqttserver ?>">
                                        <input type="text" name="mqtt_host" class="form-control input-default " value="<?php echo $m->mqtt_host ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_user" class="form-control input-default " value="<?php echo $m->mqtt_user ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Password</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_password" class="form-control input-default " value="<?php echo $m->mqtt_password ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT TCP Port</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_tcp_port" class="form-control input-default " value="<?php echo $m->mqtt_port ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Websocket Port</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_websock_port" class="form-control input-default " value="<?php echo $m->mqtt_web_port ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT TLS</label>
                                    <div class="col-sm-10">
                                        <select name="mqtt_tls" class="form-control">
                                            <option>true</option>
                                            <option>false</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Topic</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_topic" class="form-control input-default " value="<?php echo $m->mqtt_topic ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">MQTT Vendor</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mqtt_vendor" class="form-control input-default " value="<?php echo $m->mqtt_vendor ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Serial Device</label>
                                    <div class="col-sm-10">
                                        <select name="serial_device" class="form-control">
                                        <?php 
                                            foreach($devices as $d){ 
                                        ?>
                                            <option><?php echo $d->serial ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Edit</button>
                                    <a href="<?php echo base_url(). 'admin/mqtt/'?>"><button type="button" class="btn btn-warning m-b-10 m-l-5">Cancel</button></a>
                                </div>
                            </form>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>