<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">MQTT Server</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage MQTT Server</a></li>
                <li class="breadcrumb-item active">Details MQTT Server</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>MQTT Server Details</h4>
                    </div>
                    <div class="card-body">
                    <?php foreach($mqtt_server as $m){ ?>
                        <div class="form-group">
                            <label>Url</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_host ?>">
                        </div>
                        <div class="form-group">
                            <label>User</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_user ?>">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_password ?>">
                        </div>
                        <div class="form-group">
                            <label>TCP Port</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_port ?>">
                        </div>
                        <div class="form-group">
                            <label>Websocket Port</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_web_port ?>">
                        </div>
                        <div class="form-group">
                            <label>TLS</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_tls ?>">
                        </div>
                        <div class="form-group">
                            <label>Topic</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_topic ?>">
                        </div>
                        <div class="form-group">
                            <label>Vendor</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->mqtt_vendor ?>">
                        </div>
                        <div class="form-group">
                            <label>Serial Device</label>
                            <input class="form-control" type="text" disabled="" value="<?php echo $m->serial_device ?>">
                        </div>
                        <div class="form-group">
                            <a href="<?php echo base_url(). 'admin/mqtt/edit/' .$m->id_mqttserver; ?>"><button type="button" class="btn btn-warning m-b-10 m-l-5">Edit</button></a>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>