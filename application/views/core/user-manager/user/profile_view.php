<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_user.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Profile</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">User Profile</li>
            </ol>
        </div>
    </div>
    <?php 
        foreach($user as $u){ 
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <header>
                                <div class="avatar">
                                    <img src="<?php echo base_url(); ?>assets/images/users.svg" alt="user" class="profile-pic" />
                                </div>
                            </header>
                            <h3><?php echo $u->first_name ?> <?php echo $u->last_name ?></h3>
                            <div class="desc">
                                <?php echo $u->email ?> | <?php echo $u->phone ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#password" role="tab">Edit Password</a> </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>City</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $u->city ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Province</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $u->province ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Country</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $u->country ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Postcode</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $u->postcode ?></p>
                                    </div>
                                </div>
                                <hr>
                                <strong>Address</strong>
                                <br>
                                <p class="m-t-30"><?php echo $u->address ?></p>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="<?php echo base_url(). 'user/profile/exec_profile'; ?>" method="post">
                                    <hr>
                                    <input type="hidden" name="id_users" value="<?php echo $u->id_users ?>">
                                    <div class="form-group">
                                        <label class="col-md-12">Username</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control form-control-line" value="<?php echo $u->username ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">First Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="first_name" class="form-control form-control-line" value="<?php echo $u->first_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Last Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="last_name" class="form-control form-control-line" value="<?php echo $u->last_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="email" class="form-control form-control-line" value="<?php echo $u->email ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone</label>
                                        <div class="col-md-12">
                                            <input type="text" name="phone" class="form-control form-control-line" value="<?php echo $u->phone ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-12">
                                            <input type="text" name="address" class="form-control form-control-line" value="<?php echo $u->address ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">City</label>
                                        <div class="col-md-12">
                                            <input type="text" name="city" class="form-control form-control-line" value="<?php echo $u->city ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Province</label>
                                        <div class="col-md-12">
                                            <input type="text" name="province" class="form-control form-control-line" value="<?php echo $u->province ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Country</label>
                                        <div class="col-md-12">
                                            <input type="text" name="country" class="form-control form-control-line" value="<?php echo $u->country ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Postcode</label>
                                        <div class="col-md-12">
                                            <input type="text" name="postcode" class="form-control form-control-line" value="<?php echo $u->postcode ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="password" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="<?php echo base_url(). 'user/profile/exec_password'; ?>" method="post">
                                    <hr>
                                    <input type="hidden" name="id_users" value="<?php echo $u->id_users ?>">
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" name="password" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Update Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

<?php
    $this->load->view('inc/footer.php');
?>