<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Profile</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">User Profile</li>
            </ol>
        </div>
    </div>
    <?php 
        foreach($users as $u){ 
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <header>
                                <div class="avatar">
                                    <img src="<?php echo base_url(); ?>assets/images/users.svg" alt="user" class="profile-pic" />
                                </div>
                            </header>
                            <h3><?php echo $u->first_name ?> <?php echo $u->last_name ?></h3>
                            <div class="desc">
                                <?php echo $u->email ?> | <?php echo $u->phone ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Users Details</h4>
                    </div>
                    <div class="card-body">
                        <hr>
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>City</strong>
                                <br>
                                <p class="text-muted"><?php echo $u->city ?></p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Province</strong>
                                <br>
                                <p class="text-muted"><?php echo $u->province ?></p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Country</strong>
                                <br>
                                <p class="text-muted"><?php echo $u->country ?></p>
                            </div>
                            <div class="col-md-3 col-xs-6"> <strong>Postcode</strong>
                                <br>
                                <p class="text-muted"><?php echo $u->postcode ?></p>
                            </div>
                        </div>
                        <hr>
                        <strong>Address</strong>
                        <br>
                        <p class="m-t-30"><?php echo $u->address ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

<?php
    $this->load->view('inc/footer.php');
?>