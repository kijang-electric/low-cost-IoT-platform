<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('inc/sidebar_admin.php');
?>

<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Devices</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage Devices</a></li>
                <li class="breadcrumb-item active">Register Device</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Fill this form to register a devices</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form action="<?php echo base_url(). 'admin/devices/exec_register'; ?>" method="post">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Product Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="product" class="form-control input-default " placeholder="product name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Serial</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="serial" class="form-control input-default " placeholder="00-0000-0000">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    $this->load->view('inc/footer.php');
?>