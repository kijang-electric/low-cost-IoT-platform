<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_monitoring extends CI_Model{
	function get_devices_users($where,$table){		
		return $this->db->get_where($table,$where);
    }
    
    function details_monitoring_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
}