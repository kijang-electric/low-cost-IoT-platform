<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_users extends CI_Model{
	function get_users_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	public function add_user($data){
        return $this->db->insert('users', $data);
    }

	function delete_users_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function update_info_user($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function update_password_user($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function details_users_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
}