<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_mqtt extends CI_Model{
	function view_mqtt_data(){
		return $this->db->get('mqtt_server');
	}

	function insert_mqtt_data($data,$table){
		$this->db->insert($table,$data);
	}

	function delete_mqtt_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function edit_mqtt_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function details_mqtt_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_mqtt_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
}