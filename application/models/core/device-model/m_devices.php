<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_devices extends CI_Model{
	function view_devices_data(){
		return $this->db->get('devices');
	}

	function view_devices_by_user($where,$table){
		return $this->db->get_where($table,$where);
	}

	function insert_devices_data($data,$table){
		$this->db->insert($table,$data);
	}

	function delete_devices_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function edit_devices_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_devices_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
}