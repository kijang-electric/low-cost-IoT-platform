<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_vendor extends CI_Model{
	function view_vendor_data(){
		return $this->db->get('vendor');
	}

	function update_vendor_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
    
	function insert_vendor_data($data){
		return $this->db->insert('vendor', $data);
	}
}