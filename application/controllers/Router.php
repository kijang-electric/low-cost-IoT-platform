<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Router extends CI_Controller {
    function __construct(){
        parent::__construct();		
        $this->load->model('core/vendor-model/m_vendor');
    }

    public function index(){
        $install_status = $this->m_vendor->view_vendor_data()->result();

		$logged_in = $this->session->userdata('logged_in');
        $role = $this->session->userdata('role');

        if ($install_status == null) {
            redirect(base_url() . 'install/');
        } else {
            if (!$logged_in) {
                redirect(base_url() . 'login/');
            } else {
                if ($role == "admin") {
                    redirect(base_url() . 'admin/dashboard/');
                } else {
                    redirect(base_url() . 'user/dashboard/');
                }
            }
        }
	}
}