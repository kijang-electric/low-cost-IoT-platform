<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//prepare API for third-party app
class Api extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('core/m_login');
        $this->load->model('core/user-model/m_users');
        $this->load->model('core/device-model/m_devices');
        $this->load->model('sample/m_monitoring');
    }

    /**
        Administrative Method
    **/

    public function print_response($status,$info){
        $response = array(
            'Success' => $status,
            'Info' => $info);
        $this->print_json($response);
    }

    public function print_json($response){
        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
    }

    /**
        Router Method
    **/

    public function test(){
        $this->print_response(true,'Just Test !');
        exit;
    }

    public function login(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $email = $data['email'];
        $password = $data['password'];
        $where = array('email' => $email);
        $check_login = $this->m_login->checkLogin($where, $password);

        if($check_login == true){
            $getuserinfo = $this->m_login->getUserInfo($where)->result();
            
            foreach($getuserinfo as $u){
                $username=$u->username;
                $role=$u->role;
            }

            $this->print_login_response($username,true,'Logged in !');
        }else{
            $this->print_response(false,'Wrong Username or Password!');
        }

        exit;
    }

    public function print_login_response($id_user,$status,$info){
        $response = array(
            'id_user' => $id_user,
            'Success' => $status,
            'Info' => $info);
        $this->print_json($response);
    }

    public function registerUser(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $username = $data['username'];
        $first_name = $data['first_name'];
        $last_name = $data['last_name'];
        $email = $data['email'];
        $random_salt = hash('sha256', uniqid(openssl_random_pseudo_bytes(16), TRUE));
        $password = $data['password'];
        $password_salt = hash('sha256', $password . $random_salt);
        $role = "user";

        $data = [
            'username' => $username, 
            'email' => $email, 
            'first_name' => $first_name, 
            'last_name' => $last_name, 
            'password' => $password_salt, 
            'salt' => $random_salt, 
            'role' => $role,
            'date_joined' => date('Y-m-d H:i:s')
        ];

        $register_user = $this->m_users->add_user($data);

        if($register_user){
            $this->print_response(true,'Success Register Data');
        }else{
            $this->print_response(false,'Failed Register Data');
        }

        exit;       
    }

    public function detailUser(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $username = $data['username'];
        $where = array('username' => $username);
		$response = $this->m_users->details_users_data($where,'users')->result();
        
        foreach($response as $r){
            $email=$r->email;
            $first_name=$r->first_name;
            $last_name=$r->last_name;
            $address=$r->address;
            $city=$r->city;
            $province=$r->province;
            $country=$r->country;
            $postcode=$r->postcode;
            $phone=$r->phone;
        }

        $this->print_user_response($email,$first_name,$last_name,$address,$city,$province,$country,$postcode,$phone);
        exit;  
    }

    public function print_user_response($email,$first_name,$last_name,$address,$city,$province,$country,$postcode,$phone){
        $response = array(
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
            'postcode' => $postcode,
            'phone' => $phone
        );
        $this->print_json($response);
    }

    public function registerDevice(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $serial = $data['serial'];
		$username = $data['username'];

		$data = array(
			'username' => $username,
			'date_registered' => date('Y-m-d H:i:s')
		);

		$where = array(
			'serial' => $serial
		);

		$register_device = $this->m_devices->update_devices_data($where,$data,'devices');
        if($register_device){
            $this->print_response(true,'Success Register Data');
        }else{
            $this->print_response(false,'Failed Register Data');
        }
        exit;       
    }

    public function viewAllDevice(){
        $data = (array)json_decode(file_get_contents('php://input'));
		$username = $data['username'];
		$where = array('username' => $username);
        $response = $this->m_monitoring->get_devices_users($where,'devices')->result();
        $this->print_json($response);
        exit;       
    }

    public function detailDevice(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $serial = $data['serial'];
		$username = $data['username'];
		$where = array(
            'serial' => $serial,
            'username' => $username
        );
        $response = $this->m_devices->view_devices_by_user($where,'devices')->result();

        foreach($response as $r){
            $serial=$r->serial;
            $username=$r->username;
            $product=$r->product;
        }

        $this->print_device_response($serial,$username,$product);
        exit;       
    }

    public function print_device_response($serial,$username,$product){
        $response = array(
            'serial' => $serial,
            'username' => $username,
            'product' => $product
        );
        $this->print_json($response);
    }

    public function monDetails(){
        $data = (array)json_decode(file_get_contents('php://input'));
        $serial = $data['serial'];
		$where = array('serial_device' => $serial);
        $response = $this->m_monitoring->details_monitoring_data($where,'mqtt_server')->result();

        foreach($response as $r){
            $mqtt_host=$r->mqtt_host;
            $mqtt_user=$r->mqtt_user;
            $mqtt_password=$r->mqtt_password;
            $mqtt_port=$r->mqtt_port;
            $mqtt_tls=$r->mqtt_tls;
            $mqtt_topic=$r->mqtt_topic;
            $mqtt_vendor=$r->mqtt_vendor;
        }

        $this->print_mqtt_response($mqtt_host,$mqtt_user,$mqtt_password,$mqtt_port,$mqtt_tls,$mqtt_topic,$mqtt_vendor);
    }
    
    public function print_mqtt_response($mqtt_host,$mqtt_user,$mqtt_password,$mqtt_port,$mqtt_tls,$mqtt_topic,$mqtt_vendor){
        $response = array(
            'mqtt_host' => $mqtt_host,
            'mqtt_user' => $mqtt_user,
            'mqtt_password' => $mqtt_password,
            'mqtt_port' => $mqtt_port,
            'mqtt_tls' => $mqtt_tls,
            'mqtt_topic' => $mqtt_topic,
            'mqtt_vendor' => $mqtt_vendor
        );
        $this->print_json($response);
    }
}