<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends UserInterface {
	public function index(){
		$this->load->view('core/dashboard/user/dashboard_view');
	}

	public function docs(){
		$this->load->view('core/dashboard/user/documentation_view');
	}
}
