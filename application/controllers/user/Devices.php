<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devices extends UserInterface {
    function __construct(){
		parent::__construct();		
		$this->load->model('core/device-model/m_devices');
	}

	public function index(){
		$username = $this->session->userdata('username');

		$where = array(
			'username' => $username
		);

		$data['devices'] = $this->m_devices->view_devices_by_user($where, 'devices')->result();
		$this->load->view('core/device-manager/user/devices_view', $data);
	}
	
	function register(){
		$this->load->view('core/device-manager/user/devices_input_view');
	}
	
	function exec_register(){
		$serial = $this->input->post('serial');
		$username = $this->session->userdata('username');

		$data = array(
			'username' => $username,
			'date_registered' => date('Y-m-d H:i:s')
		);

		$where = array(
			'serial' => $serial
		);

		$this->m_devices->update_devices_data($where,$data,'devices');
		redirect('user/devices/');
	}
}