<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('core/m_login');
    }

    public function index() {
        $logged_in = $this->session->userdata('logged_in');
    
        if($logged_in){
            if ($role == "admin") {
                redirect(base_url().'admin/dashboard/'); 
            } else {
                redirect(base_url().'user/dashboard/');
            } 
        }

        $this->load->view('core/login_view');
    }

    public function exec_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $where = array('email' => $email);
        $check_login = $this->m_login->checkLogin($where, $password);
 
        if ($check_login == true) {
            $getuserinfo = $this->m_login->getUserInfo($where)->result();
            
            foreach($getuserinfo as $u){
                $username=$u->username;
                $role=$u->role;
            }

            $data_session = array(
                'email' => $email,
                'username' => $username,
                'role' => $role,
                'logged_in' => TRUE
            );

            $this->session->set_userdata($data_session);

            if ($role == "admin") {
                redirect(base_url().'admin/dashboard/'); 
            } else {
                redirect(base_url().'user/dashboard/');
            } 
        } else {
            $this->session->set_userdata('logged_in', false);
            $this->session->set_flashdata('msg', 'Email / Password Invalid');
            redirect(base_url().'login/');            
        }
    }
 
    public function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect(base_url().'login/');
    }
}