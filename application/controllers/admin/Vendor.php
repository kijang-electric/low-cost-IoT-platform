<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends AdminInterface {
  function __construct(){
    parent::__construct();		
    $this->load->model('core/vendor-model/m_vendor');
  }

  public function index(){
    $data['vendor'] = $this->m_vendor->view_vendor_data()->result();
    $this->load->view('core/vendor-manager/admin/vendor_view', $data);
  }

  public function license(){
    $data['vendor'] = $this->m_vendor->view_vendor_data()->result();
    $this->load->view('core/vendor-manager/admin/vendor_lic_view', $data);
  }

  public function license_edit(){
    $data['vendor'] = $this->m_vendor->view_vendor_data()->result();
    $this->load->view('core/vendor-manager/admin/vendor_lic_edit_view', $data);
  }

  public function privacy(){
    $data['vendor'] = $this->m_vendor->view_vendor_data()->result();
    $this->load->view('core/vendor-manager/admin/vendor_pri_view', $data);
  }

  public function privacy_edit(){
    $data['vendor'] = $this->m_vendor->view_vendor_data()->result();
    $this->load->view('core/vendor-manager/admin/vendor_pri_edit_view', $data);
  }

  function exec_vendor(){
    $id = $this->input->post('v_id');
		$name = $this->input->post('v_name');
		$email = $this->input->post('v_email');
		$owner = $this->input->post('v_owner');
		$address = $this->input->post('v_address');
		$city = $this->input->post('v_city');
    $province = $this->input->post('v_province');
    $country = $this->input->post('v_country');
    $postcode = $this->input->post('v_postcode');
    $phone = $this->input->post('v_phone');

		$data = array(
			'v_name' => $name,
			'v_email' => $email,
			'v_owner' => $owner,
			'v_address' => $address,
      'v_city' => $city,
      'v_province' => $province,
      'v_country' => $country,
      'v_postcode' => $postcode,
      'v_phone' => $phone
    );
    
    $where = array(
			'v_id' => $id
    );
    
		$this->m_vendor->update_vendor_data($where,$data,'vendor');
		redirect('admin/vendor/');
  }

  function exec_license(){
    $id = $this->input->post('v_id');
    $license = $this->input->post('v_license');
    
    $data = array(
			'v_license' => $license,
    );
    
    $where = array(
			'v_id' => $id
    );
    
		$this->m_vendor->update_vendor_data($where,$data,'vendor');
		redirect('admin/vendor/license/');
  }

  function exec_privacy(){
    $id = $this->input->post('v_id');
    $policy = $this->input->post('v_policy');
    
    $data = array(
			'v_policy' => $policy,
    );
    
    $where = array(
			'v_id' => $id
    );
    
		$this->m_vendor->update_vendor_data($where,$data,'vendor');
		redirect('admin/vendor/privacy/');
  }
}