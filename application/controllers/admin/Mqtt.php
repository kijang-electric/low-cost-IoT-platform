<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mqtt extends AdminInterface {
    function __construct(){
		parent::__construct();		
		$this->load->model('core/mqtt-model/m_mqtt');
		$this->load->model('core/device-model/m_devices');
    }
    
	public function index(){
        $data['mqtt_server'] = $this->m_mqtt->view_mqtt_data()->result();
		$this->load->view('core/mqtt-manager/admin/mqtt_view', $data);
    }
    
    public function details($id){
        $where = array('id_mqttserver' => $id);
		$data['mqtt_server'] = $this->m_mqtt->details_mqtt_data($where,'mqtt_server')->result();
		$this->load->view('core/mqtt-manager/admin/mqtt_details_view', $data);
	}

	function register(){
		$data['devices'] = $this->m_devices->view_devices_data()->result();
		$this->load->view('core/mqtt-manager/admin/mqtt_input_view', $data);
	}
	
	function exec_register(){
		$mqtt_host = $this->input->post('mqtt_host');
		$mqtt_user = $this->input->post('mqtt_user');
		$mqtt_password = $this->input->post('mqtt_password');
		$mqtt_tcp_port = $this->input->post('mqtt_tcp_port');
		$mqtt_websock_port = $this->input->post('mqtt_websock_port');
		$mqtt_tls = $this->input->post('mqtt_tls');
		$mqtt_topic = $this->input->post('mqtt_topic');
		$mqtt_vendor = $this->input->post('mqtt_vendor');
		$serial_device = $this->input->post('serial_device');

		$data = array(
			'mqtt_host' => $mqtt_host,
			'mqtt_user' => $mqtt_user,
			'mqtt_password' => $mqtt_password,
			'mqtt_port' => $mqtt_tcp_port,
			'mqtt_web_port' => $mqtt_websock_port,
			'mqtt_tls' => $mqtt_tls,
			'mqtt_topic' => $mqtt_topic,
			'mqtt_vendor' => $mqtt_vendor,
			'serial_device' => $serial_device
		);
		$this->m_mqtt->insert_mqtt_data($data,'mqtt_server');
		redirect('admin/mqtt/');
	}

	function delete($id){
		$where = array('id_mqttserver' => $id);
		$this->m_mqtt->delete_mqtt_data($where,'mqtt_server');
		redirect('admin/mqtt/');
	}

	function edit($id){
		$where = array('id_mqttserver' => $id);
		$data['mqtt_server'] = $this->m_mqtt->edit_mqtt_data($where,'mqtt_server')->result();
		$data['devices'] = $this->m_devices->view_devices_data()->result();
		$this->load->view('core/mqtt-manager/admin/mqtt_edit_view',$data);
	}

	function update(){
		$id = $this->input->post('id_mqttserver');
		$mqtt_host = $this->input->post('mqtt_host');
		$mqtt_user = $this->input->post('mqtt_user');
		$mqtt_password = $this->input->post('mqtt_password');
		$mqtt_tcp_port = $this->input->post('mqtt_tcp_port');
		$mqtt_websock_port = $this->input->post('mqtt_websock_port');
		$mqtt_tls = $this->input->post('mqtt_tls');
		$mqtt_topic = $this->input->post('mqtt_topic');
		$mqtt_vendor = $this->input->post('mqtt_vendor');
		$serial_device = $this->input->post('serial_device');

		$data = array(
			'mqtt_host' => $mqtt_host,
			'mqtt_user' => $mqtt_user,
			'mqtt_password' => $mqtt_password,
			'mqtt_port' => $mqtt_tcp_port,
			'mqtt_web_port' => $mqtt_websock_port,
			'mqtt_tls' => $mqtt_tls,
			'mqtt_topic' => $mqtt_topic,
			'mqtt_vendor' => $mqtt_vendor,
			'serial_device' => $serial_device
		);
	 
		$where = array(
			'id_mqttserver' => $id
		);
	 
		$this->m_mqtt->update_mqtt_data($where,$data,'mqtt_server');
		redirect('admin/mqtt/');
	}
}