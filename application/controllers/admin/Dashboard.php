<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AdminInterface {
	public function index(){
		$this->load->view('core/dashboard/admin/dashboard_view');
	}

	public function docs(){
		$this->load->view('core/dashboard/admin/documentation_view');
	}
}
