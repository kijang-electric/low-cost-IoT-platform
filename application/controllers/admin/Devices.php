<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devices extends AdminInterface {
    function __construct(){
		parent::__construct();		
		$this->load->model('core/device-model/m_devices');
	}

	public function index(){
		$data['devices'] = $this->m_devices->view_devices_data()->result();
		$this->load->view('core/device-manager/admin/devices_view', $data);
	}
	
	function register(){
		$this->load->view('core/device-manager/admin/devices_input_view');
	}
	
	function exec_register(){
		$product = $this->input->post('product');
		$serial = $this->input->post('serial');

		$data = array(
			'product' => $product,
			'serial' => $serial
		);
		$this->m_devices->insert_devices_data($data,'devices');
		redirect('admin/devices/');
	}

	function delete($id){
		$where = array('id_device' => $id);
		$this->m_devices->delete_devices_data($where,'devices');
		redirect('admin/devices/');
	}

	function edit($id){
		$where = array('id_device' => $id);
		$data['devices'] = $this->m_devices->edit_devices_data($where,'devices')->result();
		$this->load->view('core/device-manager/admin/devices_edit_view',$data);
	}

	function update(){
		$id = $this->input->post('id_device');
		$product = $this->input->post('product');
		$serial = $this->input->post('serial');
	 
		$data = array(
			'product' => $product,
			'serial' => $serial
		);
	 
		$where = array(
			'id_device' => $id
		);
	 
		$this->m_devices->update_devices_data($where,$data,'devices');
		redirect('admin/devices/');
	}
}