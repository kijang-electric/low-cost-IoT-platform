<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listmon extends AdminInterface {
	public function __construct() {
		parent::__construct();
		$this->load->model('core/device-model/m_devices');
		$this->load->model('sample/m_monitoring');
	}
		
	public function index(){
		$data['devices'] = $this->m_devices->view_devices_data()->result();
		$this->load->view('sample/admin/monitoring-device_list_view',$data);
	}

	public function details($serial){
		$where = array('serial_device' => $serial);
		$data['mqtt_server'] = $this->m_monitoring->details_monitoring_data($where,'mqtt_server')->result();
		$this->load->view('sample/inc/mqtt_list_system', $data);
		$this->load->view('sample/admin/monitoring-list_view');
	}
}