<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends AdminInterface {
    function __construct(){
        parent::__construct();		
        $this->load->model('core/user-model/m_users');
    }

    public function index(){
        $where = array('role' => 'user');
        $data['users'] = $this->m_users->get_users_data($where,'users')->result();
        $this->load->view('core/user-manager/admin/users_view', $data);
    }
    
    public function admin(){
        $where = array('role' => 'admin');
        $data['users'] = $this->m_users->get_users_data($where,'users')->result();
        $this->load->view('core/user-manager/admin/users_view', $data);
    }

    function delete($id){
		$where = array('id_users' => $id);
		$this->m_users->delete_users_data($where,'users');
		redirect('admin/users/');
    }
    
    public function details($id){
        $where = array('id_users' => $id);
		$data['users'] = $this->m_users->details_users_data($where,'users')->result();
		$this->load->view('core/user-manager/admin/users_details_view', $data);
    }
    
    function register(){
		$this->load->view('core/user-manager/admin/admin_register_view');
	}
	
	function exec_register(){
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
        $password = $this->input->post('password');
        $random_salt = hash('sha256', uniqid(openssl_random_pseudo_bytes(16), TRUE));
        $password_salt = hash('sha256', $password . $random_salt);
		$address = $this->input->post('address');
		$city = $this->input->post('city');
        $province = $this->input->post('province');
        $country = $this->input->post('country');
        $postcode = $this->input->post('postcode');
        $phone = $this->input->post('phone');
        $role = $this->input->post('role');

		$data = array(
			'username' => $username,
			'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'password' => $password_salt,
			'salt' => $random_salt,
			'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
            'postcode' => $postcode,
            'phone' => $phone,
            'role' => $role,
            'date_joined' => date('Y-m-d H:i:s')
		);
		$this->m_user->add_user($data);
		redirect('admin/users/admin/');
	}
}