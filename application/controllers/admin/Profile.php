<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends AdminInterface {
  function __construct(){
    parent::__construct();		
    $this->load->model('core/user-model/m_users');
  }

  public function index(){
    $email = $this->session->userdata('email');
    $where = array('email' => $email);
    $data['user'] = $this->m_users->get_users_data($where,'users')->result();
    $this->load->view('core/user-manager/admin/profile_view', $data);
  }

  function exec_profile(){
    $id = $this->input->post('id_users');
		$email = $this->input->post('email');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
    $province = $this->input->post('province');
    $country = $this->input->post('country');
    $postcode = $this->input->post('postcode');
    $phone = $this->input->post('phone');

		$data = array(
			'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'address' => $address,
      'city' => $city,
      'province' => $province,
      'country' => $country,
      'postcode' => $postcode,
      'phone' => $phone
    );
    
    $where = array(
			'id_users' => $id
    );
    
		$this->m_users->update_info_user($where,$data,'users');
		redirect('admin/profile/');
  }
  
  function exec_password(){
    $id = $this->input->post('id_users');
    $password = $this->input->post('password');
    $random_salt = hash('sha256', uniqid(openssl_random_pseudo_bytes(16), TRUE));
    $password_salt = hash('sha256', $password . $random_salt);

		$data = array(
			'password' => $password_salt,
			'salt' => $random_salt
    );
    
    $where = array(
			'id_users' => $id
    );

		$this->m_users->update_password_user($where,$data,'users');
		redirect('admin/profile/');
	}
}