<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {
    function __construct(){
        parent::__construct();		
        $this->load->model('core/vendor-model/m_vendor');
        $this->load->model('core/user-model/m_users');
    }

	public function index(){
		$this->load->view('core/installer_view');
    }

    public function exec_vendor() {
        $this->form_validation->set_rules('v_id', 'Vendor ID', 'required');
        $this->form_validation->set_rules('v_name', 'Vendor Name', 'required');
        $this->form_validation->set_rules('v_owner', 'Vendor Owner', 'required');
        $this->form_validation->set_rules('v_email', 'Vendor Email', 'required');
        $this->form_validation->set_rules('v_phone', 'Vendor Phone', 'required');
        $this->form_validation->set_rules('v_address', 'Vendor Address', 'required');
        $this->form_validation->set_rules('v_city', 'Vendor City', 'required');
        $this->form_validation->set_rules('v_province', 'Vendor Province', 'required');
        $this->form_validation->set_rules('v_country', 'Vendor Country', 'required');
        $this->form_validation->set_rules('v_postcode', 'Vendor Postcode', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'install/');
        } else {
            $v_id = $this->input->post('v_id');
            $v_name = $this->input->post('v_name');
            $v_owner = $this->input->post('v_owner');
            $v_email = $this->input->post('v_email');
            $v_phone = $this->input->post('v_phone');
            $v_address = $this->input->post('v_address');
            $v_city = $this->input->post('v_city');
            $v_province = $this->input->post('v_province');
            $v_country = $this->input->post('v_country');
            $v_postcode = $this->input->post('v_postcode');

            $data = [
                'v_id' => $v_id, 
                'v_name' => $v_name, 
                'v_owner' => $v_owner, 
                'v_email' => $v_email, 
                'v_phone' => $v_phone, 
                'v_address' => $v_address, 
                'v_city' => $v_city,
                'v_province' => $v_province,
                'v_country' => $v_country,
                'v_postcode' => $v_postcode,
                'v_date_installed' => date('Y-m-d H:i:s')
            ];

            $insert_data = $this->m_vendor->insert_vendor_data($data);

            if ($insert_data) {
                $this->session->set_flashdata('msg', 'Successfully Add Vendor!');
                redirect(base_url() . 'install/account/');
            }
        }
    }
    
    public function account(){
		$this->load->view('core/installer_account_view');
    }

    public function exec_admin() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
        $this->form_validation->set_message('is_unique', 'Email already exists.');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'install/account/');
        } else {
            $address = $this->input->post('address');
            $city = $this->input->post('city');
            $province = $this->input->post('province');
            $country = $this->input->post('country');
            $postcode = $this->input->post('postcode');
            $phone = $this->input->post('phone');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $random_salt = hash('sha256', uniqid(openssl_random_pseudo_bytes(16), TRUE));
            $password = $this->input->post('password');
            $password_salt = hash('sha256', $password . $random_salt);
            $role = $this->input->post('role');

            $data = [
                'username' => $username, 
                'email' => $email, 
                'address' => $address, 
                'city' => $city,
                'province' => $province, 
                'country' => $country,
                'postcode' => $postcode,
                'phone' => $phone,
                'first_name' => $first_name, 
                'last_name' => $last_name, 
                'password' => $password_salt, 
                'salt' => $random_salt, 
                'role' => $role,
                'date_joined' => date('Y-m-d H:i:s')
            ];

            $insert_data = $this->m_users->add_user($data);

            if ($insert_data) {
                $this->session->set_flashdata('msg', 'Successfully Register Admin, Login now!');
                redirect(base_url() . 'install/last/');
            }
        }
    }
    
    public function last(){
		$this->load->view('core/installer_last_view');
	}
}
