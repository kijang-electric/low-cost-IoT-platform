<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Register extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('core/user-model/m_users');
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->view('core/register_view');
    }

    public function exec_register() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
        $this->form_validation->set_message('is_unique', 'Email already exists.');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'register');
        } else {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $random_salt = hash('sha256', uniqid(openssl_random_pseudo_bytes(16), TRUE));
            $password = $this->input->post('password');
            $password_salt = hash('sha256', $password . $random_salt);
            $role = $this->input->post('role');

            $data = [
                'username' => $username, 
                'email' => $email, 
                'first_name' => $first_name, 
                'last_name' => $last_name, 
                'password' => $password_salt, 
                'salt' => $random_salt, 
                'role' => $role,
                'date_joined' => date('Y-m-d H:i:s')
            ];

            $insert_data = $this->m_users->add_user($data);

            if ($insert_data) {
                $this->session->set_flashdata('msg', 'Successfully Register, Login now!');
                redirect(base_url() . 'login/');
            }
        }
    }
}